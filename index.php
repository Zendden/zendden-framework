<?php

/**
 * Author: Glazunov Borislav (@Zendden)
 * Email: glazunov2142@icloud.com
 * 
 * GitLab: https://gitlab.com/Zendden
 * StackOverflow: https://ru.stackoverflow.com/users/372277/borislav
 */

require __DIR__ . '/vendor/autoload.php';

use app\core\Logger;
use app\core\Router;

/**
 * Starting session.
 */
session_start();


/**
 * Includes paths
 * 
 * @var string $configPath | Path to main config
 */
$configPath = realpath(__DIR__ . '\app\config\router.config.php');

/**
 * App properties
 * 
 * @var array $routerConfig
 * @var array $uriParts
 * @var array $params
 */
$routerConfig = [];
$uriParts     = [];
$params       = [];

try {
    if (file_exists($configPath)) {
        $routerConfig = require_once($configPath);

        if (is_array($routerConfig) && !empty($routerConfig)) {
            $uriParts = Router::getUriParts();
            $params   = Router::getParams();
        } else {
            throw new \Exception('Router config is empty.', 500);
        }
    } else {
        throw new \Exception('Router config is not found.', 500);
    }

    $targetController = !empty($uriParts[0])
        ? $routerConfig['pathPrefix'] . ucfirst($uriParts[0]) . $routerConfig['namingPostfix']
        : $routerConfig['pathPrefix'] . $routerConfig['defaultController'] . $routerConfig['namingPostfix'];

    $targetAction = !empty($uriParts[1])
        ? lcfirst($uriParts[1])
        : $routerConfig['defaultAction'];

    if (class_exists($targetController)) {
        /**
         * This `checkAccess` method checks access to the route.
         * Return: true or false or redirect, if the second param is true, third param is uri for redirect.
         * 
         */
        if ($targetController::checkAccess($targetController::getRole(), true)) {
            $controller = new $targetController();

            if (method_exists($controller, $targetAction)) {
                $controller->$targetAction($params ? $params : null);
            } else {
                throw new \Exception('Method [' . $targetAction . '] of class [' . $targetController . '] is not exist.', 404);
            }
        }
    } else {
        throw new \Exception('Class [' . $targetController . '] not found.', 404);
    }
} catch (\Exception $exception) {
    if ($exception->getCode() == 404) {
        Logger::write("Not found: " . $exception->getMessage() . "; Code: " . $exception->getCode(), 'main');
    } else if ($exception->getCode() == 500) {
        Logger::write("Internal server error: " . $exception->getMessage() . "; Code: " . $exception->getCode(), 'main');
    } else if ($exception->getCode() == 403) {
        Logger::write("Access denied: " . $exception->getMessage() . "; Code: " . $exception->getCode(), 'access');
    }
} catch (\PDOException $exception) {
    Logger::write("Database error: " . $exception->getMessage() . "; Code: " . $exception->getCode(), 'database');
}
