-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июл 19 2020 г., 15:08
-- Версия сервера: 10.3.13-MariaDB-log
-- Версия PHP: 7.3.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

/* Drop if exists Database */
DROP DATABASE IF EXISTS `zendden_db`;

/* Create database */
CREATE DATABASE `zendden_db`;

/* Use database */
USE `zendden_db`;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `testtask`
--

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL COMMENT 'Уникальный идентификатор заказа',
  `user_id` int(11) NOT NULL COMMENT 'Идентификатор пользователя',
  `price` int(11) NOT NULL COMMENT 'Цена заказа',
  `create_date` datetime DEFAULT current_timestamp() COMMENT 'Дата создания заказа',
  `update_date` datetime DEFAULT NULL ON UPDATE current_timestamp() COMMENT 'Дата обновления заказа'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица схем автобусов';

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `price`, `create_date`, `update_date`) VALUES
(1, 2, 100, '2020-07-19 17:02:13', NULL),
(2, 2, 150, '2020-07-19 17:02:19', NULL),
(3, 2, 150, '2020-07-19 17:02:39', NULL),
(4, 3, 500, '2020-07-19 17:02:39', NULL),
(5, 2, 150, '2020-07-19 17:02:41', NULL),
(6, 3, 500, '2020-07-19 17:02:41', NULL),
(7, 2, 1000, '2020-07-19 17:02:56', NULL),
(8, 2, 750, '2020-07-19 17:02:56', NULL),
(9, 2, 1000, '2020-07-19 17:02:58', NULL),
(10, 2, 750, '2020-07-19 17:02:58', NULL),
(11, 3, 290, '2020-07-19 17:03:41', NULL),
(12, 3, 350, '2020-07-19 17:03:41', NULL),
(13, 3, 290, '2020-07-19 17:03:43', NULL),
(14, 3, 350, '2020-07-19 17:03:43', NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL COMMENT 'Уникальный идентификатор пользователя',
  `login` varchar(55) NOT NULL COMMENT 'Логин пользователя (уникальный)',
  `email` varchar(55) NOT NULL COMMENT 'Email пользователя',
  `create_date` datetime DEFAULT current_timestamp() COMMENT 'Дата регистарции пользователя',
  `update_date` datetime DEFAULT NULL ON UPDATE current_timestamp() COMMENT 'Дата обновления пользователя',
  `password` varchar(255) NOT NULL COMMENT 'Пароль пользователя'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица пользователей';

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `login`, `email`, `create_date`, `update_date`, `password`) VALUES
(1, 'Jared', 'jaredjared', '2020-07-19 11:38:46', NULL, '$2y$10$yc718fkYhUMXv.5WxSXeYuV00xLoEX8M.STjE03sEwm1doK9U1Tzm'),
(2, 'Finally', 'jaredjared', '2020-07-19 11:41:22', NULL, '$2y$10$JbZujbWgTaGzjeIUFfKbKON5AYT2m/Cav1.8kONczHFeknnuds/.S'),
(3, 'FinallyFinnaly', 'jaredjared', '2020-07-19 11:42:30', NULL, '$2y$10$v/bW5UuhARcIk4utPPLxj.RVxo38kBEMGt1OCj2P9CoyiQ3XUAdwq'),
(5, 'NewDayLogin', 'jaredjared', '2020-07-19 11:45:12', NULL, '$2y$10$z373.RW9XNzV6ww6NosoW.BDAthIkXxnDPI1zpfLrsiU9DrYRiCa2'),
(6, 'Jaredson', 'jaredjared', '2020-07-19 11:48:37', NULL, '$2y$10$DofuRI2qdIH1jh9Egec2oexLSwviGCacAwNwjv9RtHq07FVXmZqEW'),
(7, 'JaredJared', 'JaredJared', '2020-07-19 13:19:42', NULL, '$2y$10$65DwSRUTcxY3Q2OClvZ/dOZBKs5KzECL2vT3kebwgw3qLFoQF/uCG'),
(8, 'Jaredij', 'Jared1@1.ru', '2020-07-19 13:32:03', '2020-07-19 14:58:00', '$2y$10$DwJFJvjaI/u7Jcyl5Oxxp./R5v1yql86kd3HXpm0SqJ/0sw05xgT6');

-- --------------------------------------------------------

--
-- Структура таблицы `users_attributes`
--

CREATE TABLE `users_attributes` (
  `id` int(11) NOT NULL COMMENT 'Уникальный идентификатор аттрибутов пользователя',
  `user_id` int(11) NOT NULL COMMENT 'Идентификатор пользователя',
  `first_name` varchar(255) DEFAULT NULL COMMENT 'Имя пользователя',
  `last_name` varchar(255) DEFAULT NULL COMMENT 'Фамилия пользователя',
  `middle_name` varchar(255) DEFAULT NULL COMMENT 'Отчество пользователя',
  `create_date` datetime DEFAULT current_timestamp() COMMENT 'Дата создания',
  `update_date` datetime DEFAULT NULL ON UPDATE current_timestamp() COMMENT 'Дата обновления'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Таблица базовых цен туров';

--
-- Дамп данных таблицы `users_attributes`
--

INSERT INTO `users_attributes` (`id`, `user_id`, `first_name`, `last_name`, `middle_name`, `create_date`, `update_date`) VALUES
(1, 6, 'Jared', 'Jaredson', NULL, '2020-07-19 11:48:37', NULL),
(2, 7, 'Jared', 'Jaredson', NULL, '2020-07-19 13:19:42', NULL),
(3, 8, 'JaredijJared', 'JaredsonJared', 'MiddleJackson', '2020-07-19 13:32:03', '2020-07-19 17:04:56');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `login` (`login`);

--
-- Индексы таблицы `users_attributes`
--
ALTER TABLE `users_attributes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор заказа', AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор пользователя', AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `users_attributes`
--
ALTER TABLE `users_attributes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Уникальный идентификатор аттрибутов пользователя', AUTO_INCREMENT=4;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `order_ifk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;

--
-- Ограничения внешнего ключа таблицы `users_attributes`
--
ALTER TABLE `users_attributes`
  ADD CONSTRAINT `users_attributes_ibf_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
