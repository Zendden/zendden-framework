<?php

namespace app\src\models;

use app\src\Prototypes\ModelPrototype;

/**
 * UserModel - this class provide possibility to get info from other Entities with different conditios. 
 */
class InfoModel extends ModelPrototype
{
    /**
     * Only dublicated emails.
     * 
     * MySQL more faster than PHP, then it's mean, that difficult query is better than re-sorting data. (Obviously if request is possible).
     *
     * @return array|null
     */
    public function getDublicatedEmails()
    {
        return $this->buildSelectQuery(
            'users',
            [
                'select' => [
                    'email',
                    'COUNT(email) as email_counter'
                ],
                'group' => [
                    'email'
                ],
                'having' => [
                    "email_counter > 1"
                ]
            ]
        );
    }

    public function getLoginsWithoutOrders()
    {
        return $this->query(
            "SELECT `t_users`.`login`, COUNT(`t_orders`.`id`) as `order_counter`
                FROM `users` as t_users 
                LEFT JOIN `orders` as t_orders ON `t_users`.`id` = `t_orders`.`user_id` 
            WHERE `order_counter` = 0;",
            [],
            'fetchAll'
        );
    }

    public function getLoginsByOrdersNum($codition = "=", $ordersNum = 0)
    {
        return $this->query(
            "SELECT `t_users`.`login` as `login`, COUNT(`t_orders`.`id`) as `order_counter`
                FROM `users` as t_users 
                LEFT JOIN `orders` as t_orders ON `t_users`.`id` = `t_orders`.`user_id` 
            GROUP BY `login`
            HAVING `order_counter` {$codition} {$ordersNum};",
            [],
            'fetchAll'
        );
    }
}
