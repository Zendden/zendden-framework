<?php

namespace app\src\models;

use app\src\Prototypes\ModelPrototype;

/**
 * UserModel - this class represents the User entity. 
 */
class UserModel extends ModelPrototype
{
    public function getUserData($login)
    {
        return array_shift($this->query(
            "SELECT * FROM `users` WHERE `login` = '{$login}'",
            [
                'login' => $login
            ],
            'fetchAll'
        ));
    }

    public function login($login, $password)
    {
        $userData = $this->getUserData($login);

        if ($userData) {
            if ($userData['login'] === $login && password_verify($password, $userData['password'])) {
                $this->deleteUserSession();
                $this->setUserSession($userData['id']);

                return true;
            }
        }

        return false;
    }

    public function register($login, $password, $email, $fields)
    {
        $inputAttrsTitles = [];

        if (is_array($fields) && !empty($fields)) {
            foreach ($fields as $fieldTitle => $fieldValue) {
                if (!is_null($fieldValue) && !empty($fieldValue)) {
                    $inputAttrsTitles[] = ":" . $fieldTitle;
                } else {
                    unset($fields[$fieldTitle]);
                }
            }
        }

        $this->query(
            "INSERT INTO `users` (login, email, password) VALUES (:login, :email, :password)",
            [
                'login'     => $login,
                'email'     => $email,
                'password'  => password_hash($password, PASSWORD_DEFAULT)
            ]
        );

        $insertedId = $this->connection->lastInsertId();

        if (intval($insertedId)) {
            if (!empty($inputAttrsTitles)) {
                $inputAttrsTitles[] = ":user_id";
                $fields['user_id']  = $insertedId;

                $this->query("INSERT INTO `users_attributes` (" . str_replace(':', '', implode(',', $inputAttrsTitles)) . ") VALUES (" . implode(',', $inputAttrsTitles) . ")", $fields);
            }

            $this->deleteUserSession();
            $this->setUserSession($insertedId);

            return true;
        }

        return false;
    }

    public function update($newPassword = NULL, $newEmail = NULL, $fields = [])
    {
        $userSessionData = $this->getUserSession();

        if (!empty($userSessionData)) {
            $userDbData = $this->getUserData($userSessionData['login']);

            if (!is_null($userDbData)) {
                if ($userSessionData['password'] === $userDbData['password'] && $userSessionData['login'] === $userDbData['login']) {

                    if (!is_null($newPassword)) {
                        $this->buildUpdateQuery(
                            'users',
                            [
                                'password' => password_hash($newPassword, PASSWORD_DEFAULT),
                            ],
                            [
                                'id'       => $userDbData['id']
                            ]
                        );
                    }

                    if (!is_null($newEmail)) {
                        $this->buildUpdateQuery(
                            'users',
                            [
                                'email' => $newEmail,
                            ],
                            [
                                'id'    => $userDbData['id']
                            ]
                        );
                    }

                    if (is_array($fields) && !empty($fields)) {
                        $inputAttrsTitles = [];

                        if (is_array($fields) && !empty($fields)) {
                            foreach ($fields as $fieldTitle => $fieldValue) {
                                if (!is_null($fieldValue) && !empty($fieldValue)) {
                                    $inputAttrsTitles[] = " {$fieldTitle} = :{$fieldTitle}";
                                } else {
                                    unset($fields[$fieldTitle]);
                                }
                            }
                        }

                        if (!empty($inputAttrsTitles)) {
                            $fields['user_id'] = $userDbData['id'];
                            $this->query("UPDATE `users_attributes` SET" . implode(',', $inputAttrsTitles) . " WHERE user_id = :user_id;", $fields);
                        }
                    }

                    $this->deleteUserSession();
                    $this->setUserSession($userDbData['id']);

                    return true;
                }
            }
        }

        return false;
    }

    public function setUserSession($userId)
    {
        $userData = $this->query(
            "SELECT `t_user`.*, 
                    `t_user_attributes`.`first_name`,
                    `t_user_attributes`.`last_name`,
                    `t_user_attributes`.`middle_name`,
                    `t_user_attributes`.`create_date`,
                    `t_user_attributes`.`update_date`
            FROM `users` as t_user
                LEFT JOIN `users_attributes` as t_user_attributes ON `t_user`.`id` = `t_user_attributes`.`user_id`
            WHERE `t_user`.`id` = '{$userId}'",
            [],
            'fetchAll'
        );

        if (!is_null($userData)) {
            $_SESSION['SESSIONDATA'][self::USER] = hash('crc32b', self::USER) . hash('adler32', self::USER);
            $_SESSION['SESSIONDATA']['role']     = self::USER;
            $_SESSION['SESSIONDATA']['user']     = array_shift($userData);

            return true;
        }

        return false;
    }

    public function getUserSession()
    {
        if (array_key_exists('SESSIONDATA', $_SESSION) && array_key_exists('user', $_SESSION['SESSIONDATA'])) {
            return $_SESSION['SESSIONDATA']['user'];
        }

        return [];
    }

    public function deleteUserSession()
    {
        if (array_key_exists('SESSIONDATA', $_SESSION) && array_key_exists('user', $_SESSION['SESSIONDATA'])) {
            unset($_SESSION['SESSIONDATA']);
        }

        if (!array_key_exists('SESSIONDATA', $_SESSION)) {
            return true;
        }

        return false;
    }
}
