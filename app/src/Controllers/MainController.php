<?php

namespace app\src\Controllers;

use app\src\Prototypes\ControllerPrototype;

/**
 * Main controller class
 */
class MainController extends ControllerPrototype
{
    /**
     * Access propertie.
     */
    protected static $role = self::GUEST;

    /**
     * Index method (main point for entrance).
     *
     * @return void
     */
    public function index()
    {
        $this->render(['title' => 'Main page']);
    }
}
