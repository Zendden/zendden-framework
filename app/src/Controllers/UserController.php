<?php

namespace app\src\Controllers;

use app\src\Prototypes\ControllerPrototype;
use app\src\Models\UserModel;

/**
 * User controller class
 */
class UserController extends ControllerPrototype
{
    /**
     * Access propertie.
     */
    protected static $role = self::GUEST;

    /**
     * Default proxy method.
     *
     * @return void
     */
    public function index()
    {
        self::httpRedirect('user/login');
    }

    /**
     * Login method (authorization).
     *
     * @return void
     */
    public function login()
    {
        self::checkAccess(self::GUEST);

        $login    = $this->getPost('login');
        $password = $this->getPost('password');

        if (!is_null($login) && !is_null($password)) {
            $userModel = new UserModel();

            if ($userModel->getUserData($login)) {
                if ($userModel->login($login, $password)) {
                    self::httpRedirect('user/view');
                }
            }
        }

        $this->render([
            'title' => 'Login page'
        ]);
    }

    /**
     * Register method (registration).
     *
     * @return void
     */
    public function register()
    {
        self::checkAccess(self::GUEST);

        $login      = $this->getPost('login');
        $password   = $this->getPost('password');
        $passwordRe = $this->getPost('passwordRetype');
        $email      = $this->getPost('email');
        $firstName  = $this->getPost('firstName');
        $lastName   = $this->getPost('lastName');
        $middleName = $this->getPost('middleName');

        if (!is_null($login) && !is_null($password) && !is_null($passwordRe) && !is_null($email) && ($password === $passwordRe)) {
            if ((new UserModel())->register(
                $login,
                $password,
                $email,
                [
                    'first_name'  => $firstName,
                    'last_name'   => $lastName,
                    'middle_name' => $middleName
                ]
            )) {
                self::httpRedirect('user/view');
            }
        }

        $this->render([
            'title' => 'Register page'
        ]);
    }

    /**
     * Update method (updating).
     * 
     * @return void
     */
    public function update()
    {
        self::checkAccess(self::USER, true, 'user/register');

        $password   = $this->getPost('password');
        $email      = $this->getPost('email');
        $firstName  = $this->getPost('firstName');
        $lastName   = $this->getPost('lastName');
        $middleName = $this->getPost('middleName');

        if (!is_null($password) || !is_null($firstName) || !is_null($lastName) || !is_null($middleName) || !is_null($email)) {
            if ((new UserModel())->update(
                $password,
                $email,
                [
                    'first_name'  => $firstName,
                    'last_name'   => $lastName,
                    'middle_name' => $middleName
                ]
            )) {
                self::httpRedirect('user/view');
            }
        }

        $this->render([
            'title' => 'Update profile',
            'description' => 'Here, you can change your personal data'
        ]);
    }

    public function view()
    {
        self::checkAccess(self::USER, true);

        $this->render([
            'title'       => 'Profile page',
            'description' => 'Your personal profile page, glad to see you on my site'
        ]);
    }

    /**
     * Logout method (exit).
     *
     * @return void
     */
    public function logout()
    {
        if ((new UserModel())->deleteUserSession()) {
            self::httpRedirect('main/index');
        }

        self::httpRedirect('user/login');
    }
}
