<?php

namespace app\src\Controllers;

use app\src\Prototypes\ControllerPrototype;
use app\src\Models\InfoModel;

/**
 * Info controller class.
 */
class InfoController extends ControllerPrototype
{
    /**
     * Access propertie.
     */
    protected static $role = self::USER;

    /**
     * Index method (main point for entrance).
     *
     * @return void
     */
    public function index()
    {
        if (self::checkAccess(self::USER, true)) {
            $this->showDublicatedEmails();
        }
    }

    /**
     * Dublicated emails method.
     *
     * @return void
     */
    public function showDublicatedEmails()
    {
        $this->checkAccess(self::USER, true);

        $emails = (new InfoModel())->getDublicatedEmails();

        $this->render([
            'title'  => 'Dublicated emails into database',
            'emails' => !empty($emails) ? $emails : []
        ]);
    }

    /**
     * Without orders.
     *
     * @return void
     */
    public function showHaventOrders()
    {
        $this->checkAccess(self::USER, true);

        $logins = (new InfoModel())->getLoginsByOrdersNum('=', 0);

        $this->render([
            'title'  => 'Logins without orders',
            'logins' => $logins ? $logins : []
        ]);
    }

    /**
     * With more than two orders.
     *
     * @return void
     */
    public function showHaveMoreThanTwoOrders()
    {
        $this->checkAccess(self::USER, true);

        $logins = (new InfoModel())->getLoginsByOrdersNum('>', 2);

        $this->render([
            'title'  => 'Logins with more than two orders',
            'logins' => $logins ? $logins : []
        ]);
    }
}
