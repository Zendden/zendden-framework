<?php

namespace app\src\Prototypes;

use app\core\Logger;
use app\core\Render;

/**
 * Class ControllerPrototype, all controllers must extands of this class.
 */
class ControllerPrototype
{
    /**
     * Roles for access control and clarifing Routes visibility.
     */
    const USER  = 'USER';
    const GUEST = 'GUEST';

    /**
     * Proxy-method.
     * 
     * @param array $param
     * @return void
     */
    protected function render($param = [])
    {
        (new Render())->render($param);
    }

    /**
     * Service method for prepare link before redirect.
     * 
     * @param string $url
     * @return string
     */
    public static function prepareUrl($url)
    {
        $validUrl = false;

        /**
         * Checking the correct type of URL for redirect, 
         * if necessary, then concatenate the protocol
         * and cutting extra slashes.
         */
        if ((preg_match("[(http://{$_SERVER['HTTP_HOST']})]", $url, $matches)) === 1) {
            $validUrl = filter_var($url, FILTER_VALIDATE_URL);
        } elseif (preg_match("[(https://)]", $url, $matches) === 1) {
            $validUrl = filter_var(preg_replace("[(https://)]", "http://", $url), FILTER_VALIDATE_URL);
        } elseif ((preg_match("[({$_SERVER['HTTP_HOST']}/)|({$_SERVER['HTTP_HOST']})]", $url, $matches)) === 1) {
            $validUrl = filter_var("http://$url", FILTER_VALIDATE_URL);
        } else {
            $validUrl = filter_var("http://{$_SERVER['HTTP_HOST']}/" . ltrim($url, '/'), FILTER_VALIDATE_URL);
        }

        if (!is_bool($validUrl)) {
            return $validUrl;
        } else {
            throw new \Exception('Bad request, url is uncorrect.', 500);
        }
    }

    /**
     * This method take any type url and redirected with help of HTTP headers.
     * 
     * @param string $url
     * @return void
     */
    public static function httpRedirect($url)
    {
        header(
            "Location: " . self::prepareUrl($url)
        );
    }

    /**
     * Method, which control user access to any module.
     * 
     * @param string $role
     * @return bool
     */
    public static function checkAccess($role, $redirect = false, $redirectUrl = "")
    {
        try {
            if (isset($_SESSION['SESSIONDATA'])) {
                /**
                 * @var string $role    | Role, which passed from controller
                 * @var array  $roles   | Roles-list for check availability $role from controller into $roles array
                 */
                $role  = strtoupper($role);
                $roles = require(realpath('app\config\access.config.php')); //Requiring array with roles-list ['GUEST','USER'];

                /**
                 * Checking user session array for availability the Role and the secret-key variables.
                 */
                if (array_key_exists($role, $roles)) {
                    foreach ($roles as $userRole => $roleWeight) {
                        if (array_key_exists($userRole, $_SESSION['SESSIONDATA'])) {
                            /**
                             * If user-role have more weight than controller-role, then access granted.
                             */
                            if ($roles[$userRole] >= $roles[$role]) {
                                return true;
                            }
                        }
                    }
                } else {
                    throw new \Exception('Such role is not exist. Please, contact with administrator.', 403);
                }
            } else {
                $_SESSION['SESSIONDATA'][self::GUEST] = hash('crc32b', self::GUEST) . hash('adler32', self::GUEST);
                $_SESSION['SESSIONDATA']['role']      = self::GUEST;
            }

            if ($redirect) {
                $url = "/";
                if (!empty($redirectUrl)) {
                    $url = $redirectUrl;
                }

                self::httpRedirect($url);
            }
        } catch (\Exception $exception) {
            Logger::write("Access denied: " . $exception->getMessage() . "; Code: " . $exception->getCode(), 'access');
        }

        return false;
    }

    /**
     * Extract $_POST variables
     *
     * @param string $variable
     * @return mixed|null
     */
    public static function getPost($variable)
    {
        if (array_key_exists($variable, $_POST) && !empty($_POST[$variable])) {
            return strip_tags($_POST[$variable]);
        }

        return null;
    }

    /**
     * Getter of access propertie.
     */
    public static function getRole()
    {
        return (get_called_class())::$role;
    }
}
