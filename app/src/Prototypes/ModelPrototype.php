<?php

namespace app\src\Prototypes;

use app\core\Database;
use app\core\Logger;

/**
 * Class ModelPrototype, all models must extands of this class.
 */
class ModelPrototype
{
    /**
     * Roles for access control and clarifing Routes visibility.
     */
    const USER  = 'USER';
    const GUEST = 'GUEST';

    /**
     * Default property, which handle object of PDO into yourself.
     * 
     * @var \PDO|null $connection
     */
    protected $connection = null;

    /**
     * Query method to Database.
     *
     * @param string $sql
     * @param array $params
     * @param string $fetchMethod
     * @return mixed
     */
    public function query(string $sql, array $params = [], string $fetchMethod = null)
    {
        try {
            $this->connection = Database::getConnection();

            $query = $this->connection->prepare($sql);

            if (!empty($params)) {
                foreach ($params as $paramName => $paramValue) {
                    $paramType = is_int($paramValue) ? \PDO::PARAM_INT : \PDO::PARAM_STR;

                    $query->bindValue(":{$paramName}", $paramValue, $paramType);
                }
            }

            $query->execute();
        } catch (\PDOException $exception) {
            die($exception->getMessage());
        }

        if (is_null($fetchMethod)) {
            return $query;
        } else {
            $result = $query->$fetchMethod();
            return $result;
        }
    }

    /**
     * Select builder.
     *
     * @param string $targetTable
     * @param array $condition
     * @param integer $limit
     * @return array|null
     */
    public function buildSelectQuery(string $targetTable, array $parameters = [], $alias = "table")
    {
        $sql = "SELECT ";

        if (array_key_exists('select', $parameters) && !empty($parameters['select'])) {
            $sql .= implode(',', $parameters['select']);
        } else {
            $sql .= "*";
        }

        if (!empty($targetTable)) {
            $sql .= " FROM {$targetTable} as {$alias}_{$targetTable} ";
        } else {
            throw new \PDOException('Target table or expression, which imitate table is not defined.');
        }

        if (array_key_exists('join', $parameters) && !empty($parameters['join'])) {
            $sql .= implode(' ', $parameters['join']);
        }

        if (array_key_exists('where', $parameters) && !empty($parameters['where'])) {
            $sql .= " WHERE " . implode(' AND ', $parameters['where']);
        }

        if (array_key_exists('group', $parameters) && !empty($parameters['group'])) {
            $sql .= " GROUP BY " . implode(',', $parameters['group']);
        }

        if (array_key_exists('having', $parameters) && !empty($parameters['having'])) {
            $sql .= " HAVING " . implode(' AND ', $parameters['having']);
        }

        if (array_key_exists('order', $parameters) && !empty($parameters['order'])) {
            $sql .= " ORDER BY " . implode(',', $parameters['order']);
        }

        if (array_key_exists('limit', $parameters) && !empty($parameters['limit'])) {
            $sql .= " LIMIT " . intval(implode('', $parameters['limit']));
        }

        $sql .= ";";

        try {
            return $this->query($sql, [], 'fetchAll');
        } catch (\PDOException $exception) {
            Logger::write($exception->getMessage(), 'database');
        }
    }

    /**
     * Insert builder.
     *
     * @param string $targetTable
     * @param array $parameters
     * @return bool|int
     */
    public function buildInsertQuery(string $targetTable, array $rawParameters)
    {
        $parameters = [];

        $parametersTitles = [];
        if (is_array($rawParameters) && !empty($rawParameters)) {
            foreach ($rawParameters as $title => $value) {
                if (!intval($title) && !empty($value) && !is_null($value)) {
                    $parametersTitles[$title] = ":{$title}";

                    $parameters[$title] = $value;
                }
            }
        }

        try {
            if (!empty($parametersTitles) && !empty($parameters) && (count($parametersTitles) === count($parameters)) && !empty($targetTable)) {
                if (!empty($targetTable)) {
                    $this->query(
                        "INSERT INTO `{$targetTable}` (" . str_replace(':', '', implode(',', $parametersTitles)) . ") VALUES (" . implode(',', $parametersTitles) . ");",
                        $parameters
                    );

                    return intval($this->connection->lastInsertId());
                }

                throw new \PDOException('Target table or expression, which imitate table is not defined.');
            }
        } catch (\PDOException $exception) {
            Logger::write($exception->getMessage(), 'database');
        }

        return false;
    }

    /**
     * Update bilder.
     *
     * @param string $targetTable
     * @param array $parameters
     * @param array $conditions
     * @return bool
     */
    public function buildUpdateQuery(string $targetTable, array $rawParameters, array $rawConditions)
    {
        $parameters = [];

        $updatedColumns = [];
        if (is_array($rawParameters) && !empty($rawParameters)) {
            foreach ($rawParameters as $title => $value) {
                if (!intval($title) && !empty($value) && !is_null($value)) {
                    $updatedColumns[] = "`{$title}` = :{$title}";

                    $parameters[$title] = $value;
                }
            }
        }

        $conditions = [];
        if (is_array($rawConditions) && !empty($rawConditions)) {
            foreach ($rawConditions as $title => $value) {
                if (!intval($title) && !empty($value) && !is_null($value)) {
                    $conditions[] = "`$title` = :{$title}";

                    $parameters[$title] = $value;
                }
            }
        }

        try {
            if (!empty($targetTable)) {
                if (!empty($updatedColumns)) {
                    if (!empty($conditions)) {
                        $this->query("UPDATE `{$targetTable}` SET " . implode(',', $updatedColumns) . " WHERE " . implode(',', $conditions) . ";", $parameters);

                        return true;
                    } else {
                        throw new \PDOException('Unconditional updation request may harm the database, please specify the terms exactly.');
                    }
                }
            } else {
                throw new \PDOException('Target table or expression, which imitate table is not defined.');
            }
        } catch (\PDOException $exception) {
            Logger::write($exception->getMessage(), 'database');
        }

        return false;
    }

    /**
     * Delete builder.
     *
     * @param string $targetTable
     * @param array $rawConditions
     * @return bool
     */
    public function buildDeleteQuery(string $targetTable, array $rawConditions)
    {
        $parameters = [];

        $conditions = [];
        if (is_array($rawConditions) && !empty($rawConditions)) {
            foreach ($rawConditions as $title => $value) {
                if (!intval($title) && !empty($value) && !is_null($value)) {
                    $conditions[$title] = "`{$title}` = :{$title}";

                    $parameters[$title] = $value;
                }
            }
        }

        try {
            if (!empty($targetTable)) {
                if (!empty($conditions) && !empty($parameters)) {
                    $this->query("DELETE FROM `{$targetTable}` WHERE " . implode(',', $conditions) . ";", $parameters);

                    return true;
                } else {
                    throw new \PDOException('Unconditional deletion request may harm the database, please specify the terms exactly.');
                }
            } else {
                throw new \PDOException('Target table or expression, which imitate table is not defined.');
            }
        } catch (\PDOException $exception) {
            Logger::write($exception->getMessage(), 'database');
        }

        return false;
    }
}
