<h2>Обо мне</h2>

<hr>

<div class="container">
    <p>Занимаюсь разработкой проектов как нативно, так и на фреймворке Symfony 4 с применением сопутствующих технологий версионирования GIT, пакетного менеджера composer на PHP c базой данных MySQL.</p>

    <p>Разработка на frontend-e с применением js/jQuery, knockout.js (в качестве реактивной библиотеки), twig/blade шаблонизаторы, верстка (при необходимости) CSS3/HTML5.</p>

    <p>Администрирование Linux систем, VDS и VPS с такими популярными дистрибутивами как Fedora 29-31, Manjaro 19, Ubuntu 18.04. Установка и настройка стеков LAMP/LEMP. Скрипты на bash под автоматизированный backup базы данных.</p>
</div>