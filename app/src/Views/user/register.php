<form class="form-register" action="/user/register" method="POST" id="form">

    <h1 align="center"><?= $title ? $title : ''; ?></h1>

    <hr><br>

    <label for="inputLogin" class="sr-only">Login</label>
    <input type="text" id="inputLogin" class="form-control" placeholder="Login" name="login" required autofocus><br>

    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" id="inputEmail" class="form-control" placeholder="Email address" name="email" required><br>

    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required><br>

    <label for="inputPasswordRetype" class="sr-only">Password re-type</label>
    <input type="password" id="inputPasswordRetype" class="form-control" placeholder="Password re-type" name="passwordRetype" required><br>

    <label for="firstName" class="sr-only">Firstname</label>
    <input type="text" id="firstName" class="form-control" placeholder="Firstname" name="firstName"><br>

    <label for="lastName" class="sr-only">Lastname</label>
    <input type="text" id="lastName" class="form-control" placeholder="Lastname" name="lastName"><br>

    <label for="middleName" class="sr-only">Middlename</label>
    <input type="text" id="middleName" class="form-control" placeholder="Middlename" name="middleName"><br>

    <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit" value="sent">Register</button>

</form>