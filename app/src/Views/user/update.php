<form class="form-update" action="/user/update" method="POST" id="form">

    <h1 align="center"><?= $title ? $title : ''; ?></h1>

    <hr>

    <h3 align="center"><?= $description ? $description : ''; ?></h3><br>

    <label for="inputLogin" class="sr-only">Login</label>
    <input type="text" id="inputLogin" class="form-control" value="<?= isset($_SESSION['SESSIONDATA']['user']['login']) ? $_SESSION['SESSIONDATA']['user']['login'] : ''; ?>" name="login" readonly><br>

    <label for="inputEmail" class="sr-only">Email address</label>
    <input type="email" id="inputEmail" class="form-control" value="<?= isset($_SESSION['SESSIONDATA']['user']['email']) ? $_SESSION['SESSIONDATA']['user']['email'] : ''; ?>" name="email"><br>

    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" class="form-control" placeholder="New password" name="password"><br>

    <label for="firstName" class="sr-only">Firstname</label>
    <input type="text" id="firstName" class="form-control" value="<?= isset($_SESSION['SESSIONDATA']['user']['first_name']) ? $_SESSION['SESSIONDATA']['user']['first_name'] : ''; ?>" name="firstName"><br>

    <label for="lastName" class="sr-only">Lastname</label>
    <input type="text" id="lastName" class="form-control" value="<?= isset($_SESSION['SESSIONDATA']['user']['last_name']) ? $_SESSION['SESSIONDATA']['user']['last_name'] : ''; ?>" name="lastName"><br>

    <label for="middleName" class="sr-only">Middlename</label>
    <input type="text" id="middleName" class="form-control" value="<?= isset($_SESSION['SESSIONDATA']['user']['middle_name']) ? $_SESSION['SESSIONDATA']['user']['middle_name'] : ''; ?>" name="middleName"><br>

    <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit" value="sent">Update</button>

</form>