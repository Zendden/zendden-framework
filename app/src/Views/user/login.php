<form class="form-login" action="/user/login" method="POST" id="form">

    <h1 align="center"><?= $title ? $title : ''; ?></h1>

    <hr><br>

    <label for="inputLogin" class="sr-only">Login</label>
    <input type="text" id="inputLogin" class="form-control" placeholder="Login" name="login" required><br>

    <label for="inputPassword" class="sr-only">Password</label>
    <input type="password" id="inputPassword" class="form-control" placeholder="Password" name="password" required><br>

    <button class="btn btn-lg btn-primary btn-block" type="submit" name="submit" value="sent">Log In</button>

</form>