<h1 align="center"><?= $title ? $title : ''; ?></h1>

<br>
<hr>

<h3 align="center"><?= $description ? $description : ''; ?></h3><br>


<label for="inputLogin" class="sr-only">Login</label>
<input type="text" id="inputLogin" class="form-control" value="<?= isset($_SESSION['SESSIONDATA']['user']['login']) ? $_SESSION['SESSIONDATA']['user']['login'] : ''; ?>" name="login" readonly><br>

<label for="inputEmail" class="sr-only">Email address</label>
<input type="email" id="inputEmail" class="form-control" value="<?= isset($_SESSION['SESSIONDATA']['user']['email']) ? $_SESSION['SESSIONDATA']['user']['email'] : ''; ?>" name="email" readonly><br>

<label for="firstName" class="sr-only">Firstname</label>
<input type="text" id="firstName" class="form-control" value="<?= isset($_SESSION['SESSIONDATA']['user']['first_name']) ? $_SESSION['SESSIONDATA']['user']['first_name'] : ''; ?>" name="firstName" readonly><br>

<label for="lastName" class="sr-only">Lastname</label>
<input type="text" id="lastName" class="form-control" value="<?= isset($_SESSION['SESSIONDATA']['user']['last_name']) ? $_SESSION['SESSIONDATA']['user']['last_name'] : ''; ?>" name="lastName" readonly><br>

<label for="middleName" class="sr-only">Middlename</label>
<input type="text" id="middleName" class="form-control" value="<?= isset($_SESSION['SESSIONDATA']['user']['middle_name']) ? $_SESSION['SESSIONDATA']['user']['middle_name'] : ''; ?>" name="middleName" readonly><br>

<label for="registerDate" class="sr-only">Register date</label>
<input type="text" id="registerDate" class="form-control" value="<?= isset($_SESSION['SESSIONDATA']['user']['create_date']) ? $_SESSION['SESSIONDATA']['user']['create_date'] : ''; ?>" name="createDate" readonly><br>

<label for="lastUpdateDate" class="sr-only">Last update date</label>
<input type="text" id="lastUpdateDate" class="form-control" value="<?= (isset($_SESSION['SESSIONDATA']['user']['update_date']) && !is_null($_SESSION['SESSIONDATA']['user']['update_date'])) ? $_SESSION['SESSIONDATA']['user']['update_date'] : 'Not updated'; ?>" name="lastUpdateDate" readonly><br>

<a href="/user/update"><button class="btn btn-lg btn-primary btn-block">Change profile data</button></a><br><br>