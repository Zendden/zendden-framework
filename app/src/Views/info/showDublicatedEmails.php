<h1 align="center"><?= $title ? $title : ''; ?></h1>

<br>

<table class="table table-striped">
    <thead>
        <tr>
            <th scope="col">Email</th>
            <th scope="col">Number of times</th>
        </tr>
    </thead>
    <tbody>
        <?php if (is_array($emails) && !empty($emails)) : ?>
            <?php foreach ($emails as $data) : ?>
                <tr>
                    <th><?= $data['email'] ? $data['email'] : ''; ?></th>
                    <td><?= $data['email_counter'] ? $data['email_counter'] : ''; ?></td>
                </tr>
            <?php endforeach; ?>
        <?php else : ?>
            <tr>
                <td colspan="2">
                    <h2 align="center">By such condition emails is not found</h2>
                </td>
            </tr>
        <?php endif; ?>
    </tbody>
</table>