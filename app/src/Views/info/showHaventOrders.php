<h1 align="center"><?= $title ? $title : ''; ?></h1>

<br>

<table class="table table-striped">
    <thead>
        <tr>
            <th scope="col">Logins</th>
        </tr>
    </thead>
    <tbody>
        <?php if (is_array($logins) && !empty($logins)) : ?>
            <?php foreach ($logins as $data) : ?>
                <tr>
                    <td><?= $data['login'] ? $data['login'] : '';  ?></td>
                </tr>
            <?php endforeach; ?>
        <?php else : ?>
            <tr>
                <td>
                    <h2 align="center">By such condition logins is not found</h2>
                </td>
            </tr>
        <?php endif; ?>
    </tbody>
</table>