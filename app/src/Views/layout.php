<!DOCTYPE html>
<html lang="ru">


<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="theme-color" content="#563d7c">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" crossorigin="anonymous">
    <!-- CSS -->
    <link rel="stylesheet" href="/../../app/public/css/style.css" crossorigin="anonymous">
    <!-- jQUERY -->
    <script src="/../../app/public/js/jquery.js" crossorigin="anonymous"></script>
    <!-- TITLE -->
    <title>
        <?= $title ? $title : 'Test - task' ?>
    </title>
</head>


<body class="body">

    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">

            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Home</a>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">User</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown01">
                        <?php if (!isset($_SESSION['SESSIONDATA']['user']) && $_SESSION['SESSIONDATA']['role'] == 'GUEST') : ?>
                            <a class="dropdown-item" href="/user/login">Login</a>
                            <a class="dropdown-item" href="/user/register">Register</a>
                        <?php endif; ?>

                        <?php if (isset($_SESSION['SESSIONDATA']['user']) && $_SESSION['SESSIONDATA']['role'] !== 'GUEST') : ?>
                            <a class="dropdown-item" href="/user/update"><span class="btn btn-primary">Update</span></a>
                            <a class="dropdown-item" href="/user/view"><span class="btn btn-primary">Profile</span></a>
                            <hr>
                            <a class="dropdown-item" href="/user/logout"><span class="btn btn-danger">Log Out</span></a>
                        <?php endif; ?>
                    </div>
                </li>

                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="dropdown02" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Info</a>
                    <div class="dropdown-menu" aria-labelledby="dropdown02">
                        <a class="dropdown-item" href="/info/show-dublicated-emails">Dublicated emails list</a>
                        <a class="dropdown-item" href="/info/show-havent-orders">Logins without orders</a>
                        <a class="dropdown-item" href="/info/show-have-more-than-two-orders">Logins with more than two orders</a>
                    </div>
                </li>

                <?php if (isset($_SESSION['SESSIONDATA']['user']) && $_SESSION['SESSIONDATA']['role'] == 'USER') : ?>
                    <li class="nav-item">
                        <span class="nav-link">You entered as: <?= $_SESSION['SESSIONDATA']['user']['login']; ?></span>
                    </li>
                <?php endif; ?>
            </ul>

        </div>
    </nav>


    <main role="main">

        <!-- Main jumbotron for a primary marketing message or call to action -->
        <div class="jumbotron">
            <div class="container">
                <h1 class="display-4" align="center">Test task</h1>
                <hr>
                <h2 class="lead" align="center">Test task to <b>Work</b> Company.</h2>
            </div>
        </div>

        <!-- Main space for content -->
        <div class="container">
            <section>
                <?= $content ? $content : ''; ?>
            </section>
        </div>

    </main>


    <br>


    <footer class="footer-container" align="center">
        <div class="lead">
            <p class="footer-content" align="center">
                <b>&copy; GitLab</b>: @Zendden<br>
                <small>Glazunov2142@icloud.com</small>
            </p>
        </div>
    </footer>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" crossorigin="anonymous"></script>
    <script src="/../../app/public/js/scripts.js" crossorigin="anonymous"></script>
</body>

</html>