<?php

namespace app\core;

/**
 * Class Logger, provide a log-write functionality.
 */
class Logger
{
    protected static $config;

    /**
     * Get logger config.
     *
     * @param string $path | Path to config, if location changed.
     * @return array
     */
    public static function getConfig(string $customConfigPath = "")
    {
        $configPath = realpath('app\config\logger.config.php');

        if (!empty($customConfigPath) && $customConfigPath) {
            $configPath = $customConfigPath;
        }

        return require($configPath);
    }

    /**
     * Get log name from paths array.
     *
     * @param array $logPathArray
     * @return string
     */
    public static function getFileName(array $logPathArray)
    {
        if (is_array($logPathArray)) {
            $logFileName = array_pop($logFileName);
        }

        return $logFileName;
    }

    /**
     * Check directories, if not exists, than create.
     *
     * @param string $logPath
     * @return string|bool
     */
    public static function checkDirectories(string $logPath)
    {
        $logPathArray = explode('\\', $logPath);

        if (!is_array($logPathArray) || empty($logPathArray)) {
            $logPathArray = explode('/', $logPath);
        }

        $dir  = "";
        $file = "";
        if (is_array($logPathArray) && !empty($logPathArray)) {
            self::getFileName($logPathArray);

            foreach ($logPathArray as $logPathPart) {
                if (!is_dir($dir .= "/{$logPathPart}")) {
                    mkdir($dir);
                }
            }

            return "{$dir}/{$file}";
        }

        return false;
    }

    /**
     * Write log to file.
     *
     * @param string $logData
     * @param string $logType
     * @return void
     */
    public static function write(string $logData, string $logType = "main")
    {
        self::$config = self::getConfig();

        if (is_array(self::$config) && !empty(self::$config)) {
            if (array_key_exists($logType, self::$config)) {

                $logPath = self::$config[$logType];

                if (!file_exists($logPath)) {
                    $logPath = self::checkDirectories($logPath);
                }

                if ($logPath && file_exists($logPath)) {
                    $fd = fopen(realpath($logPath), 'a');
                    fwrite($fd, (self::prepareLog($logData)));
                    fclose($fd);
                }
            }
        }
    }

    /**
     * Create a log format.
     *
     * @param string $logData
     * @return string
     */
    public static function prepareLog(string $logData)
    {
        return "{\r\nIP: " . $_SERVER['REMOTE_ADDR'] . "\r\n Who: " . $_SERVER['HTTP_USER_AGENT'] . "\r\n Data: " . $logData . "\r\n}\r\n\r\n";
    }
}
