<?php

namespace app\core;

/**
 * Class Render, service.
 */
class Render
{
    /**
     * Properties.
     * 
     * @var array  $path
     * @var string $controller
     * @var string $action
     * @var array  $renderConfigig
     */
    public    $path         = null;
    public    $controller   = null;
    public    $action       = null;
    protected $renderConfig = null;

    /**
     * Exploding such path `main/index` to array $this->path = [0 => 'main', 1 => 'index'];
     */
    public function __construct()
    {
        $this->renderConfig = require_once(realpath('app\config\render.config.php'));

        /**
         * To camlCase for unix-like systems.
         * 
         * Handle such uri-parts, example: site.ru/hello-world-uri-part and lead to such value: site.ru/helloWorldUriPart
         */
        if (strripos($_GET['uri'], '-')) {
            $uriNamingParts = explode('-', $_GET['uri']);

            if (is_array($uriNamingParts) && !empty($uriNamingParts)) {
                $uriNamingParts = array_map(function ($item) {
                    return ucfirst($item);
                }, $uriNamingParts);
            }
        }

        $uriParts = explode('/', $_GET['uri']);

        if (is_array($uriNamingParts) && !empty($uriNamingParts)) {
            $uriParts = explode('/', implode('', $uriNamingParts));
        }

        $controllerName = array_shift($uriParts);
        $actionName     = array_shift($uriParts);

        $this->controller = (!empty($controllerName) && !is_null($controllerName)) ? $controllerName : $this->renderConfig['defaultController'];
        $this->action     = (!empty($actionName) && !is_null($actionName))         ? $actionName     : $this->renderConfig['defaultAction'];
    }

    /**
     * Render method responsible for extracting variable into `Layout`, 
     * also turning on the buffering output and unite templates into one layout. 
     * 
     * @param array $param
     * @return void
     */
    public function render(array $params = []): void
    {
        /**
         * Create the propertie {$this->path} here,
         * because this is the last step before rendering.
         * 
         * @var string $this->path
         */
        $this->path = $this->renderConfig['pathPrefix'] . '\\'  //prefix of path to file
            . strtolower($this->controller) . '\\'              //name of  controller to lowercase
            . strtolower($this->action)                         //name of `controller->*action*` to lowercase
            . $this->renderConfig['namingPostfix']              //the end prefix of naming template files
            . $this->renderConfig['defaultFileExtention'];      //default extension of your project files `.php`


        /**
         * Checking parameters array on empty,
         * if not so, then extract() it.
         */
        if (!empty($params)) {
            extract($params);
        }

        /**
         * Checking file on exist, if so,
         * then:
         *      -starting buffering
         *      -including the target layout
         *      -getting the buffering output into target variable
         *      -including the base tempalte
         * if not so, throw \Exception ();
         */
        if (file_exists($this->path)) {
            ob_start();
            include($this->path);
            $content = ob_get_clean();
            include($this->renderConfig['pathPrefix'] . '\\'
                . $this->renderConfig['defaultLayout']
                . $this->renderConfig['defaultFileExtention']);
        } else {
            throw new \Exception('Template not found' . $this->path, 500);
        }
    }
}
