<?php

return [
    'database' => 'app\\logs\\database.log.txt',
    'access'   => 'app\\logs\\access.log.txt',
    'main'     => 'app\\logs\\main.log.txt'
];
